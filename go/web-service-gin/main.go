package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"reflect"
	"strconv"
)

type person struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

var people = []person{
	{ID: 1, FirstName: "Amin", LastName: "Sheikhi"},
	{ID: 2, FirstName: "Ramin", LastName: "Pedram"},
}

func main() {
	router := gin.Default()
	router.GET("/people", getPeople)
	router.GET("/people/:id", getPersonById)

	err := router.Run("localhost:8080")
	if err != nil {
		return
	}
}

func getPeople(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, people)
}

func getPersonById(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		panic(err)
	}
	index := Find(people, func(value interface{}) bool {
		return value.(person).ID == id
	})

	person := people[index]
	c.IndentedJSON(http.StatusOK, person)
}

func Find(slice interface{}, f func(value interface{}) bool) int {
	s := reflect.ValueOf(slice)
	if s.Kind() == reflect.Slice {
		for index := 0; index < s.Len(); index++ {
			if f(s.Index(index).Interface()) {
				return index
			}
		}
	}
	return -1
}
