package main

type Product struct {
	ID int
	name string
}

func main() {
	var item Product
	item.ID = 100

	println(item)
}
