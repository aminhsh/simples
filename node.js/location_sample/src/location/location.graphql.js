import { buildSchema } from "graphql";
import { locationService } from "./location.service";

export const schema = buildSchema(`
  type Query {
    setLocation(input: CreateLocation): Location,
    locations: [Location],
    findNearest(input: FindNearestDto): [Location]
  }
  
  input CreateLocation {
    lat: Float,
    long: Float
  }
  
  input FindNearestDto {
    point: Point,
    radius: Int
  }
  
  input Point {
    lat: Float,
    long: Float
  }
  
  type Location {
    id: String,
    title: String, 
    lat: Float,
    long: Float
  }
`);

export const root = {
    setLocation({ input }) {
        return locationService.create(input);
    },
    locations() {
        return locationService.getAll();
    },
    findNearest({ input }) {
        return locationService.findNearest(input.point, input.radius)
    }
}




