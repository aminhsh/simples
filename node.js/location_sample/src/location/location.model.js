import { Schema, model } from 'mongoose';
import mongoosastic from 'mongoosastic';

const LocationSchema = new Schema({
    id: String,
    title: String,
    lat: Number,
    long: Number
});

LocationSchema.plugin(mongoosastic);

export const Location = model('Location', LocationSchema);
