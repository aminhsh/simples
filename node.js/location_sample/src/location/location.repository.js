import { Location } from "./location.model";

class LocationRepository {
    async create(data) {
        const newLocation = new Location({
            id: data.id,
            title: `Location title for id "${ data.id }"`,
            lat: data.lat,
            long: data.long
        });

        await newLocation.save();
        return newLocation;
    }
}

export const locationRepository = new LocationRepository;
