import { elasticProxy, makeId, positionHelper } from "../utils";
import { locationRepository } from "./location.repository";

class LocationService {
    create(dto) {
        // logic

        const id = makeId(5);

        return locationRepository.create({
            id,
            title: `Location title for id "${ id }"`,
            lat: dto.lat,
            long: dto.long
        });
    }

    getAll() {
        return elasticProxy.search({
            index: 'locations',
            body: {
                query: {
                    match_all: {}
                }
            }
        });
    }

    findNearest(point, radius) {
        const radiusInMeters = radius * 1000;
        const minLat = positionHelper.reduceLat(point.lat, radiusInMeters),
            maxLat = positionHelper.addLat(point.lat, radiusInMeters),
            minLong = positionHelper.reduceLong(point.lat, point.long, radiusInMeters),
            maxLong = positionHelper.addLong(point.lat, point.long, radiusInMeters);

        return elasticProxy.search({
            index: 'locations',
            body: {
                query: {
                    bool: {
                        filter: [
                            { range: { lat: { gte: minLat, lte: maxLat } } },
                            { range: { long: { gte: minLong, lte: maxLong } } }
                        ]
                    }
                }
            }
        });
    }
}

export const locationService = new LocationService;
