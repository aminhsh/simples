import express from 'express';
import bodyParser from 'body-parser';
import { schema, root } from "./location/location.graphql";
import mangoose from 'mongoose';
import { graphqlHTTP } from "express-graphql";

mangoose.connect(process.env.MONGO_URL || 'mongodb://localhost:27017/location_sample_db').then();

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use('/graphql', graphqlHTTP({
    schema,
    rootValue: root,
    graphiql: true
}));

app.listen(port, () => console.log(`App is running on port ${ port }`));






