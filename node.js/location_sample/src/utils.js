import { Client } from 'elasticsearch';

const client = new Client();

export function makeId(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export const elasticProxy = {
    search: params => {
        return new Promise(((resolve, reject) => {
            client.search(params, (err, result) => {
                if (err)
                    reject(err)
                else
                    resolve(result.hits.hits.map(e => e._source));
            })
        }));
    }
}

const earth = 6378.137,
    pi = Math.PI,
    meterInDegree = (1 / ((2 * pi / 360) * earth)) / 1000;

export const positionHelper = {
    addLat(lat, meters) {
        return lat + (meters * meterInDegree);
    },

    reduceLat(lat, meters) {
        return lat - (meters * meterInDegree);
    },

    addLong(lat , long, meters) {
        return long + (meters * meterInDegree) / Math.cos(lat * (pi / 180));
    },

    reduceLong(lat, long, meters) {
        return long - (meters * meterInDegree) / Math.cos(lat * (pi / 180));
    }
}
