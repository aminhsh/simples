const express = require('express')

const app = express()

app.get('/short', (req, res) => {
  console.log('[SHORT-REQUEST]', new Date())
  res.send(`The is response for requesting`)
})

app.get('/long', (req, res) => {
  console.log('[LONG-REQUEST-START]', new Date())

  setTimeout(() => {
    res.send(`The is response for requesting`)
    console.log('[LONG-REQUEST-END]', new Date())

  }, 10000)
})

app.listen(process.env.PORT, () => console.log('[APP-STARTED]', process.env.PORT))

const { spawn } = require('child_process')
const ls = spawn('ls', ['-lh', '.','-a'])


ls.stdout.on('readable', function() {
  const d = this.read();
  d && console.log(d.toString());
})

ls.on('close', function(code) {
  console.log('child process exited with code ' + code)
})


