const { createClient } = require('redis');

async function main() {
  const client = createClient();
  client.connect();
  
  const key = 'KING_OF_RULES_NEXT_CYCLE'

  const result = await client.set(key, JSON.stringify({ name: 'amin' }));
  const data = await client.get(key);

  console.log(result);
  console.log(data);
}

main()