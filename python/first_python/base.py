params = {'one': 'First structure', 'two': 'Another thing'}

print(params)

print(params['two'])

print(params.keys())

print(params.values())


class Person:
    """this is the first class I`ve ever written in Python language"""

    count = 0

    def __init__(self, name, code):
        self.name = name
        self.code = code
        Person.add_count()

    @staticmethod
    def add_count():
        Person.count += 1

    @staticmethod
    def display_count():
        print(Person.count)


p1 = Person('Amin', '0452698')

print(p1.count)


print(Person.count)

print('Value from status method')
Person.display_count()

print(hasattr(p1, 'name'))
print(getattr(p1, 'name'))
print(setattr(p1, 'name', 'Amin'))

print(p1.__dict__)
print(Person.__name__)
print(Person.__module__)
print(Person.__bases__)
print(Person.__doc__)


