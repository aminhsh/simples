class Person:
    __tableName__ = 'people'

    @staticmethod
    def getTableName():
        return Person.__tableName__

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary

    def __str__(self):
        return 'Amin Person'


class Order:
    def __init__(self, date):
        self.date = date


class Product:
    def __init__(self, sku):
        self.sku = sku

