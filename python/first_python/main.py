# This is a sample Python script.
from index import Person, Order, Product


# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

pr = Person("Amin", "a@b.com")
print(pr.__tableName__)
print(Person.getTableName())
print(pr.getTableName())

# newOrder = Order('134131')
# print(newOrder.date)
#
# prd = Product(100000)
# print(prd.sku)
