import pymongo

client = pymongo.MongoClient('localhost', 27017)

db = client.noteapp_db

users = db.users.find()

for user in users:
    print(f"{user['name']} => {user['email']}")
