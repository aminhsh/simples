from postgres import Postgres

db = Postgres(url='host=localhost port=5432 dbname=gop_db user=postgres password=P@ssw0rd connect_timeout=10')
result = db.all("select * from states")

for item in result:
    print(f'{item.key} => {item.name}')

